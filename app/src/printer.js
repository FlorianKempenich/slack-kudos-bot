const debug = require("debug")("kudos:printer");
const {get_async} = require("./cmd");

const extractJobName = stdout => {
  // request id is cups-pdf-3
  return /request id is (\S+)/.exec(stdout)[1];
};

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

const retryUntil = async (cb, ms) => {
  const start = new Date().getTime();
  const end = start + ms;
  let lastError;
  do {
    try {
      return await cb();
    } catch (e) {
      lastError = e;
      await wait(ms / 100);
    }
  } while (new Date().getTime() < end);

  throw new Error(
    `Retry wasn't successful in ${ms}ms. Last error was ${lastError}`
  );
};

class Printer {
  constructor(name, opts = { timeout: 20000 }) {
    debug("Initialized printer %s with %o", name, opts);
    this.name = name;
    this.opts = opts;
  }

  async print(file) {
    const lpResult = await get_async(`lp -d "${this.name}" -o fit-to-page "${file}"`);
    const jobName = extractJobName(lpResult.stdout);
    debug("Jobname is %s", jobName);

    const result = await retryUntil(async () => {
      const lpstatResult = await get_async(
        `lpstat -P "${this.name}" -W completed`
      );

      if (lpstatResult.stdout.includes(jobName)) {
        debug("Job %s completed", jobName);
        return;
      };
      debug("Job %s hasn't completed yet, waiting. %O", lpstatResult);
      throw new Error(`Could not print ${file}`);
    }, this.opts.timeout);
  }
}

module.exports = Printer;
