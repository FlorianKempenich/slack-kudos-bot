const cmd = require("node-cmd");

const get_async = command =>
  new Promise((resolve, reject) =>
    cmd.get(command, (err, stdout, stderr) => {
      if (err) reject(new Error(`Error while running ${command}:
        ${JSON.stringify({ err, stdout, stderr }, undefined, 2)}`));
      resolve({ stdout, stderr });
    })
  );

module.exports = { get_async };
