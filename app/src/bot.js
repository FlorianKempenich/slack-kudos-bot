const debug = require("debug")("kudos:bot");
const createKudos = require("./kudos");

class Bot {
  constructor({ slack, printer }) {
    this.slack = slack;
    this.printer = printer;
    this.slack.onDirectMessage(this.onDirectMessage.bind(this));
    this.queue = {};
  }

  async onDirectMessage({ channel: recipient, text: msg }) {
    debug("Received direct message from %s", recipient);
    if (this.queue[recipient] && msg === "yes") {
      try {
        await this.printer.print(this.queue[recipient]);
      } catch (e) {
        debug("Error while trying to print: %O", e);
        await this.slack.sendDirectMessage(
          recipient,
          "Something is wrong with the printer, I'm sorry."
        );
        return;
      }

      await this.slack.sendDirectMessage(
        recipient,
        "Your kudos was successfully sent to the printer, thanks!"
      );
      delete this.queue[recipient];
      debug("Removed kudos from %s from queue", recipient);
    } else {
      const { ts: timestampOfWaitMsg } = await this.slack.sendDirectMessage(
        recipient,
        "Generating..."
      );
      const { previewImage, pdf } = await createKudos(msg);
      this.queue[recipient] = pdf;
      await this.slack.sendImage(recipient, previewImage);
      await this.slack.deleteMessage(recipient, timestampOfWaitMsg);
      await this.slack.sendDirectMessage(
        recipient,
        "Here's a preview of how your kudos card will look like. If you type *yes*, I will print it. A new template is picked at random, so you can just type your message again and get another template! :bulb:"
      );
    }
  }
}

module.exports = Bot;
