const Bot = require("./bot");
const kudos = require("./kudos");

jest.mock("./kudos");

describe("Slack Kudos Bot", () => {
  let bot, slack, printer;
  beforeEach(() => {
    kudos.mockResolvedValue({
      pdf: "file.pdf",
      previewImage: "file.jpg"
    });

    slack = {
      onDirectMessage: jest.fn(),
      sendImage: jest.fn(),
      sendDirectMessage: jest.fn().mockResolvedValue({ ts: "" }),
      deleteMessage: jest.fn().mockResolvedValue()
    };

    bot = new Bot({
      slack,
      printer: (printer = { print: jest.fn() })
    });
  });

  const simulateMessage = async text =>
    await slack.onDirectMessage.mock.calls[0][0]({
      channel: "user",
      text
    });

  it("should send a preview of the kudos card to the user", async () => {
    await simulateMessage("Hello World");

    expect(slack.sendImage).toHaveBeenCalledWith("user", "file.jpg");
    expect(slack.sendDirectMessage).toHaveBeenCalledWith(
      "user",
      "Here's a preview of how your kudos card will look like. If you type *yes*, I will print it. A new template is picked at random, so you can just type your message again and get another template! :bulb:"
    );
  });

  it("should send a message right away that it's generating the kudos, that is deleted once the image is there", async () => {
    slack.sendDirectMessage.mockResolvedValue({ ts: "TIMESTAMP" });
    await simulateMessage("Hello World");

    expect(slack.sendDirectMessage).toHaveBeenCalledWith(
      "user",
      "Generating..."
    );
    expect(slack.deleteMessage).toHaveBeenCalledWith("user", "TIMESTAMP");
  });

  it("should send the kudos to the printer after receiving a yes message", async () => {
    await simulateMessage("Hello World");
    await simulateMessage("yes");

    expect(printer.print).toHaveBeenCalledWith("file.pdf");
    expect(slack.sendDirectMessage).toHaveBeenCalledWith(
      "user",
      "Your kudos was successfully sent to the printer, thanks!"
    );
  });

  it("should send a message to the user if printing fails", async () => {
    printer.print.mockRejectedValue("");

    await simulateMessage("Hello World");
    slack.sendDirectMessage.mockReset();
    await simulateMessage("yes");

    expect(slack.sendDirectMessage).toHaveBeenCalledWith(
      "user",
      "Something is wrong with the printer, I'm sorry."
    );
  });
});
