const debug = require("debug")("kudos:slack");
const { createReadStream } = require("fs");
const { WebClient } = require("@slack/web-api");
const { RTMClient } = require("@slack/rtm-api");

const wait = async ms => new Promise(resolve => setTimeout(resolve, ms));
const waitAnd = async cb => (await Promise.all([cb(), wait(1000)]))[0];

class Slack {
  constructor(token) {
    this.rtmClient = new RTMClient(token);
    this.webClient = new WebClient(token);
  }

  async init() {
    this.rtmClient.start();
    const identity = await this.webClient.auth.test();
    this.userId = identity.user_id;
    debug("Set identity to %s", this.userId);
  }

  onDirectMessage(cb) {
    this.rtmClient.on("message", event => {
      if (event.user !== this.userId && !event.hidden) {
        debug("Handling message %O", event);
        cb(event);
      } else {
        debug("Not handling message %o", {
          user: event.user,
          hidden: event.hidden,
          type: event.type,
          subtype: event.subtype
        });
      }
    });
  }

  async deleteMessage(recipient, ts) {
    return waitAnd(() =>
      this.webClient.chat.delete({
        channel: recipient,
        ts,
        as_user: true
      })
    );
  }

  async sendDirectMessage(recipient, msg) {
    return waitAnd(() =>
      this.webClient.chat.postMessage({
        channel: recipient,
        text: msg,
        mrkdown: true,
        as_user: true
      })
    );
  }

  async sendImage(recipient, image, opts) {
    this.webClient.files.upload({
      ...opts,
      channels: recipient,
      file: createReadStream(image)
    });
  }
}

module.exports = async token => {
  const slack = new Slack(token);
  await slack.init();
  return slack;
};
