const cmd = require("./cmd");
const Printer = require("./printer");

jest.mock("./cmd");

describe("Printer", () => {
  let printer;
  let create_get_async_impl;
  let lpstat_num_of_calls

  beforeEach(() => {
    printer = new Printer("name-of-printer", { timeout: 100 });
    jest.resetAllMocks();

    lpstat_num_of_calls = 0
    create_get_async_impl = ({ lpstat_result: lpstat_result }) =>
      async cmd => {
        if (cmd.startsWith("lp "))
          return { stdout: "request id is cups-pdf-3 (1 file(s))", stderr: "" };

        if (cmd.startsWith("lpstat")) {
          lpstat_num_of_calls += 1;
          return lpstat_result;
        }
      }

    lpstat_success = {
      stdout: "cups-pdf-3              root             75776   Sat 20 Apr 2019 09:57:59 PM UTC",
      stderr: ""
    }
    lpstat_failure = { stdout: "", stderr: "" }
    cmd.get_async.mockImplementation(create_get_async_impl({ lpstat_result: lpstat_success }));
  });

  it("should invoke lp to print a given file to the printer", async () => {
    await printer.print("file.pdf");
    expect(cmd.get_async).toHaveBeenCalledWith(
      `lp -d "name-of-printer" -o fit-to-page "file.pdf"`
    );
  });

  it("should resolve if the file was printed successfully", async () => {
    await printer.print("file.pdf");
    expect(cmd.get_async).toHaveBeenCalledWith(
      `lpstat -P "name-of-printer" -W completed`
    );
  });

  it("should retry if the file has not been printed yet", async () => {
    number_of_failure_before_succesful = 3
    for (let i = 0; i < number_of_failure_before_succesful; i++) {
      cmd.get_async.mockImplementationOnce(create_get_async_impl({ lpstat_result: lpstat_failure }))
    }
    cmd.get_async.mockImplementation(create_get_async_impl({ lpstat_result: lpstat_success }))

    await printer.print("file.pdf");
    expect(cmd.get_async).toHaveBeenCalledWith(
      `lpstat -P "name-of-printer" -W completed`
    );
    expect(lpstat_num_of_calls).toEqual(number_of_failure_before_succesful)
  });

  it("should fail if the file has not been printed after 2000 seconds", async () => {
    cmd.get_async.mockImplementation(create_get_async_impl({ lpstat_result: lpstat_failure }))

    try {
      await printer.print("file.pdf");
      fail();
    } catch (e) {}
  });
});
