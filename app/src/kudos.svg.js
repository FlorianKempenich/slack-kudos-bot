const debug = require("debug")("kudos:kudos.svg");
const fs = require("fs");
const { promisify } = require("util");

const glob = require("glob");
const { file } = require("tmp-promise");
const cmd = require("./cmd");

const globP = promisify(glob);
const readFileP = promisify(fs.readFile);
const writeFileP = promisify(fs.writeFile);

let templateFiles;
const randomTemplate = async () => {
  if (!templateFiles)
    templateFiles = await globP(__dirname + "/../templates/*.svg");

  return templateFiles[(Math.random() * templateFiles.length) | 0];
};

const FONT_SIZE_CHECKPOINTS = [
  [40, 38],
  [50, 36],
  [60, 32],
  [70, 30],
  [80, 28],
  [90, 26],
  [100, 24],
  [110, 22],
  [120, 20],
  [260, 17],
  [400, 15],
  [600, 12],
  [800, 10],
  [1000, 8]
];

const interpoliteSizeForLength = length => {
  if (length < 50) return 40;

  let larger, smaller;
  for (let i = 0; i < FONT_SIZE_CHECKPOINTS.length-2; i++) {
    let j = Number(i);
    if (FONT_SIZE_CHECKPOINTS[j + 1][0] >= length) {
      smaller = j;
      larger = j + 1;
      break;
    }
  }

  if (!larger) {
    debug(
      "Can't interpolate font size for message with length %d, returning min size",
      length
    );
    return FONT_SIZE_CHECKPOINTS.slice(-1)[0][1];
  }

  const [upperLimit, upperSize] = FONT_SIZE_CHECKPOINTS[larger];
  const [lowerLimit, lowerSize] = FONT_SIZE_CHECKPOINTS[smaller];

  const pxPerChar = (lowerSize - upperSize) / (upperLimit - lowerLimit);
  const lengthDelta = length - lowerLimit;

  return Math.floor(lowerSize - pxPerChar * lengthDelta);
};

const sizeForLength = length => {
  return interpoliteSizeForLength(length);
  if (length >= 599) return 12;
  if (length >= 260) return 18;
  if (length >= 120) return 20;
  if (length >= 110) return 22;
  if (length >= 100) return 24;
  if (length >= 90) return 26;
  if (length >= 80) return 28;
  if (length >= 70) return 30;
  if (length >= 60) return 32;
  if (length >= 50) return 36;
  return 40;
};

const createKudos = async msg => {
  const template = await randomTemplate();
  const templateContent = (await readFileP(template)).toString();
  const fontSize = sizeForLength(msg.length);
  debug(
    'Creating kudos on %s (length %d, font-size: %d) with content "%s"',
    template,
    msg.length,
    fontSize,
    msg
  );

  const renderedSvg = templateContent
    .replace("$TEXT", msg)
    .replace("$FONT_SIZE", fontSize);
  const { path: svgPath } = await file({ prefix: "kudos-", postfix: ".svg" });
  await writeFileP(svgPath, renderedSvg);

  const { path: pdfPath } = await file({ prefix: "kudos-", postfix: ".pdf" });
  await cmd.get_async(`inkscape -z -A "${pdfPath}" -T "${svgPath}"`);

  const { path: pngPath } = await file({ prefix: "kudos-", postfix: ".png" });
  await cmd.get_async(`inkscape -z -b white -e "${pngPath}" -T "${svgPath}"`);

  const result = { pdf: pdfPath, previewImage: pngPath };
  debug("Files are in %o", result);
  return result;
};

module.exports = createKudos;
