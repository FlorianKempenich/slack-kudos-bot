const glob = require("glob");
const fs = require("fs");
const cmd = require("./cmd");
const { file } = require("tmp-promise");
const createKudos = require("./kudos.svg");

jest.mock("tmp-promise");
jest.mock("glob");
jest.mock("fs");
jest.mock("./cmd");

describe("kudos", () => {
  beforeEach(() => {
    jest.resetAllMocks();
    glob.mockImplementation((_, cb) => cb(undefined, ["template.svg"]));
    fs.readFile.mockImplementation((_, cb) =>
      cb(undefined, `<svg><text font-size="$FONT_SIZE">$TEXT</text></svg>`)
    );
    fs.writeFile.mockImplementation((_, __, cb) => cb(undefined));

    file.mockImplementation(async ({ postfix, prefix }) => {
      return { path: `/tmp/${prefix}woop${postfix}` };
    });
  });

  it("should render the given message into a blank page", async () => {
    const msg = "Hello World";

    await createKudos(msg);

    expect(fs.writeFile).toHaveBeenCalledWith(
      "/tmp/kudos-woop.svg",
      expect.stringContaining(msg),
      expect.anything()
    );
  });

  it("should render the kudos into a temporary pdf file", async () => {
    const msg = "Hello World";

    await createKudos(msg);

    expect(cmd.get_async).toHaveBeenCalledWith(
      `inkscape -z -A "/tmp/kudos-woop.pdf" -T "/tmp/kudos-woop.svg"`
    );
  });

  it("should yield the path to a preview image and to the pdf", async () => {
    const msg = "Hello World";

    const { pdf, previewImage } = await createKudos(msg);

    expect(cmd.get_async).toHaveBeenCalledWith(
      `inkscape -z -b white -e "/tmp/kudos-woop.png" -T "/tmp/kudos-woop.svg"`
    );

    expect(pdf).toEqual("/tmp/kudos-woop.pdf");
    expect(previewImage).toEqual("/tmp/kudos-woop.png");
  });

  describe("font-size", () => {
    it("defaults to 40", async () => {
      const msg = "W".repeat(20);

      await createKudos(msg);

      expect(fs.writeFile).toHaveBeenCalledWith(
        "/tmp/kudos-woop.svg",
        expect.stringContaining(`font-size="40"`),
        expect.anything()
      );
    });

    [
      [50, 36],
      [60, 32],
      [70, 30],
      [80, 28],
      [90, 26],
      [100, 24],
      [110, 22],
      [115, 21],
      [120, 20],
      [235, 17],
      [10000, 8]
    ].forEach(([length, size]) => {
      it(`reduces the font-size to ${size} for text => than ${length}`, async () => {
        const msg = "W".repeat(length);

        await createKudos(msg);

        expect(fs.writeFile).toHaveBeenCalledWith(
          "/tmp/kudos-woop.svg",
          expect.stringContaining(`font-size="${size}"`),
          expect.anything()
        );

      })
    })
  })
});
