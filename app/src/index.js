#!/usr/bin/env node

const debug = require("debug");
debug.enable("kudos:*");

const SLACK_TOKEN = process.env.SLACK_TOKEN;

const Bot = require("./bot");
const createSlack = require("./slack");
const Printer = require("./printer");

const run = async () => {
  const slack = await createSlack(SLACK_TOKEN);
  const printer = new Printer("cups-pdf");
  const bot = new Bot({ slack, printer });
};

run();
