#!/bin/bash -e

on_chroot <<EOF
    usermod -a -G lp,lpadmin root
    /etc/init.d/cups start
    lpadmin -p cups-pdf -v cups-pdf:/ -E -P /usr/share/ppd/cups-pdf/CUPS-PDF.ppd
    lpstat -a
    /etc/init.d/cups stop
EOF
