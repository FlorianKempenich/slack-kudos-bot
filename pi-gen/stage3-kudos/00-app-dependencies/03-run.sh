#!/bin/bash -e

install -v -m 644 files/finger-paint-v7-latin-regular.ttf		"${ROOTFS_DIR}/usr/share/fonts/"
install -v -m 644 files/slack-kudos_*_all.deb "${ROOTFS_DIR}/tmp"

on_chroot <<EOF
  fc-cache -f -v
  dpkg -i /tmp/slack-kudos_*_all.deb
  usermod -a -G lp slack-kudos
EOF
